import requests
from lxml import html
from db import db
import sys


# Přihlašovací údaje
username = 'drill'
password = 'skill'

# Důležitá url
loginUrl = "https://www.drillandskill.com/cs/app/auth/login"
taskUrl = "https://www.drillandskill.com/cs/app/solve/start/1439"
processUrl = 'https://www.drillandskill.com/cs/app/solve/process_answer'
checkUrl = "https://www.drillandskill.com/cs/app/solve/checkAnswer"

# očekávané množství otázek
count_q = 5

# XPath
xpath = '/html/body/div[3]/div[2]/div/form/div[1]/div/div'
xpath = list(xpath)
xpath[15] = '2'
xpath = ''.join(xpath)

# START
# Vytvoří objekt prohlížeče
br = requests.Session()
# Login
payload = {'user_name': username, 'password': password}
br.post(loginUrl, data=payload)

r = br.get(taskUrl)
for i in range(count_q):
    sys.stderr.write(str(i)+'\n')
# Nutná změna na jiná cvičení
#############################
    # ziskání otázky
    tree = html.fromstring(r.content.decode('utf-8'))
    questions = tree.xpath(xpath)
    sorted_questions = []
    textToNum = {}
    for q in questions:
        sorted_questions.append(q[0].text.strip())
        textToNum[sorted_questions[-1]] = q[2].attrib['value']
    sorted_questions.sort()
    text_question = ';'.join(sorted_questions)
    ans_list = []
    try:
        ans_text_list = db[text_question].split(';')
        ans_list = []
        for a in ans_text_list:
            ans_list.append(textToNum[a])
    except KeyError:
        sys.stderr.write('WRONG!\n')
    post = {'item_id[]': ans_list}

# Konec změn
############
    br.post(checkUrl, data=post)
    r = br.get(processUrl)
print(r.content.decode('utf-8'))
