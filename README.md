# Bot na Drill&Skill

## Jak použít

1. Upravíš url a login v drill.py i skill.py.
2. Smažeš db.py.
3. Upravíš si hledání otázky a odpovědi, šablony v složkách:
	* img/
	* text/
	* radioButText/
	* radioButImg/
	* sort/
4. Upravíš počet očekávaného celkového množství otázek v drill.py
5. Pustíš drill.py
6. Upravíš počet otázek v skill.py.
7. Pustíš skill.py
8. Dostaneš za 1.
