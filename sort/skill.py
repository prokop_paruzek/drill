# XPath
xpath = '/html/body/div[3]/div[2]/div/form/div[1]/div/div'
xpath = list(xpath)
xpath[15] = '2'
xpath = ''.join(xpath)

# Nutná změna na jiná cvičení
#############################
    # ziskání otázky
    tree = html.fromstring(r.content.decode('utf-8'))
    questions = tree.xpath(xpath)
    sorted_questions = []
    textToNum = {}
    for q in questions:
        sorted_questions.append(q[0].text.strip())
        textToNum[sorted_questions[-1]] = q[2].attrib['value']
    sorted_questions.sort()
    text_question = ';'.join(sorted_questions)
    ans_list = []
    try:
        ans_text_list = db[text_question].split(';')
        ans_list = []
        for a in ans_text_list:
            ans_list.append(textToNum[a])
    except KeyError:
        sys.stderr.write('WRONG!\n')
    post = {'item_id[]': ans_list}

# Konec změn
############
