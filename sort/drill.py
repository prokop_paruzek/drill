# XPath
xpath = '/html/body/div[3]/div[2]/div/form/div[1]/div/div'
xpath = list(xpath)
xpath[15] = '2'
xpath = ''.join(xpath)

def run(br, out):
    # ziskání otázky
    cviceni = br.get(taskUrl)
    tree = html.fromstring(cviceni.content.decode('utf-8'))
    questions = tree.xpath(xpath)
    sorted_questions = []
    numToText = {}
    for q in questions:
        sorted_questions.append(q[0].text.strip())
        numToText[q[2].attrib['value']] = sorted_questions[-1]
    sorted_questions.sort()
    text_question = ';'.join(sorted_questions)

    # Ziskani odpovedi
    result = br.get(checkUrl)
    text_answer = result.content.decode('utf-8')
    text_answer.replace('\n', '')
    ansString = search(r'<br />([^<]*)<br />', text_answer).group(1)
    correct_answer_list = []
    for a in ansString.split(','):
        correct_answer_list.append(numToText[a])
    correct_answer = ';'.join(correct_answer_list)

    # Zkontroluje jestli v db.py neni uz ta sama otazka a vytiskne to start_cviceni
    # ve formatu python Dictionary
    if text_question in out:
        write = False
    else:
        write = True

    if write:
        print("Writing")
        out[text_question] = correct_answer
    else:
        print("Found a duplicate")
    return write
