# XPath
xpath = '/html/body/div[3]/div[2]/div/form/div[1]/p[2]/a/img'
xpath = list(xpath)
xpath[15] = '2'
xpath = ''.join(xpath)


def run(br, out):
    # ziskání otázky
    cviceni = br.get(taskUrl)
    tree = html.fromstring(cviceni.content.decode('utf-8'))
    question = tree.xpath(xpath)
    text_question = question[0].attrib['src']

    # Ziskani odpovedi
    result = br.get(checkUrl)
    text_answer = result.content.decode('utf-8')
    text_answer.replace('\n', '')
    correct_answer = search(r'<br />([^<]*)<br />', text_answer).group(1)

    # Zkontroluje jestli v db.py neni uz ta sama otazka a vytiskne to start_cviceni
    # ve formatu python Dictionary
    if text_question in out:
        write = False
    else:
        write = True

    if write:
        print("Writing")
        out[text_question] = correct_answer
    else:
        print("Found a duplicate")
    return write
